#include <Matrix.h>
#include <Sprite.h>

Matrix mtx = Matrix(10, 12, 11); // DIN, CLK, LOADの各ピン番号
double d;
int history[8];
int wind;

void setup() {
  /* initialize history */
  for (int i = 0; i < 8; i++)
    history[i] = 0;
  
  mtx.clear(); // LEDマトリックスをすべて消灯
}

void loop() {
  /* update history */
  for (int i = 7; 0 < i; i--)
    history[i] = history[i - 1];
  wind = (int)(analogRead(0) / 64);
  if (wind > 7)
    wind = 7;
  history[0] = wind;
  
  for (int j = 0; j < 8; j++) {
    if (history[j] > 0) {
      for (int i = 0; i < history[j]; i++) {
	mtx.write(i, j, HIGH); // (i,j)の位置のLEDを点灯
      }
    }
  }
  delay(50);
  mtx.clear();
}
