const int msec = 1; // 点灯時間[ms]
const int num = 8; // 行と列の大きさ
const int row[num] = {2, 3, 4, 5, 6, 7, 8, 9}; // 行方向ピンの番号（D2-D9）
const int col[num] = {12, 13, 14, 15, 16, 17, 18, 19}; // 列方向ピンの番号
                                                       //（A0-A5は14-19番に対応）
void setup() {
  for(int i = 0; i < num; i++) { // 行方向のピンの初期化
    pinMode(row[i], OUTPUT);
    digitalWrite(row[i], LOW);
  }
  for (int i = 0; i < num; i++) { // 列方向のピンの初期化
    pinMode(col[i], OUTPUT);
    digitalWrite(col[i], HIGH);
  }
}

void loop() {
  for (int i = 0; i < num; i++) {
    digitalWrite(row[i], HIGH); // 各行をHIGHに
    for (int j = 0; j < num; j++) {
      digitalWrite(col[j], LOW); // 各列をLOWに(点灯させる)
      delay(msec); // 点灯時間だけ待つ
      digitalWrite(col[j], HIGH); // 各列をHIGHに変更する(消灯状態に戻す)
    }
    digitalWrite(row[i], LOW); // 各行をLOWに
  }
}
