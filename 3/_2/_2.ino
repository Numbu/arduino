#include <Matrix.h>
#include <Sprite.h>

Matrix mtx = Matrix(10, 12, 11); // DIN, CLK, LOADの各ピン番号
double d;
void setup() {
  mtx.clear(); // LEDマトリックスをすべて消灯
}

void loop() {
  for (int k = 1; k <=13; k++) {
    for (int j = 0; j < 8; j++) {
      for (int i = 0; i < 8; i++) {
	if ((k - 1) * (k - 1) < (d = ((i - 3.5) * (i - 3.5) + (j - 3.5) * (j - 3.5)))
	    && (d <= k * k)) {
	  mtx.write(i, j, HIGH); // (i,j)の位置のLEDを点灯
	}
      }
    }
    delay(100);
    mtx.clear();
  }
}
