int  SWITCH = 13; // スイッチの入力をデジタルピン13番に接続
int  ledPin = 12; // デジタルピン12番にLEDを接続

void  setup() {
  pinMode(SWITCH, INPUT ); //  SWITCH (13番) ピンを 入力モードに
  pinMode(ledPin , OUTPUT ); // ledPin(12番ピン) を出力に使用
}

void  loop() {
  if(digitalRead(SWITCH )== HIGH) //タクトスイッチが押されたか？
    // ス イ ッ チ が 押 さ れ る と ， 回 路 が 導 通 状 態 に な り ，
    // SWITCHの入力は高電圧（HIGH)になる
    digitalWrite(ledPin , HIGH); // LEDを点灯する
  else
    digitalWrite(ledPin, LOW); // LEDを消灯する
}

