int SWITCH = 13;
int Green = 10;
int Yellow = 11;
int Red = 12;

/* keep each color state */
int Green_state;
int Yellow_state;
int Red_state;

int SW_now;
int SW_last;

long lastDebounceTime = 0;
long debounceDelay = 10;

void setup(){
  pinMode(SWITCH, INPUT);
  pinMode(Green, OUTPUT);
  pinMode(Yellow, OUTPUT);
  pinMode(Red,  OUTPUT);
  
  /* initial state is Red */
  Green_state = LOW;
  Yellow_state; LOW;
  Red_state = HIGH;
}

void loop(){
  long now = millis();

  /* chattering prevention quoted from 1-2*/
  if((now - lastDebounceTime) > debounceDelay){
    SW_now = digitalRead(SWITCH);
    lastDebounceTime = now;
    if(SW_last == LOW && SW_now == HIGH){
      onPress(); // change state
    }
    SW_last = SW_now;
  }
}

void onPress(){
  /* Green -> Yellow */
  if(Green_state == HIGH){
    digitalWrite(Green, LOW);
    digitalWrite(Yellow, HIGH);
    Green_state = LOW;
    Yellow_state = HIGH;
  }
  /* Yellow -> Red */
  else if(Yellow_state == HIGH){
    digitalWrite(Yellow, LOW);
    digitalWrite(Red, HIGH);
    Yellow_state = LOW;
    Red_state = HIGH;
  }
  /* Red -> Green */
  else {
    digitalWrite(Red, LOW);
    digitalWrite(Green, HIGH);
    Red_state = LOW;
    Green_state = HIGH;  
  }
}
