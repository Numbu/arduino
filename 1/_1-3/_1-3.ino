int ledGreen = 10;
int ledYellow = 11;
int ledRed = 12;

/* keep each color state */
int greenState;
int yellowState;
int redState;

void setup() {
  pinMode(ledGreen, OUTPUT);
  pinMode(ledYellow, OUTPUT);
  pinMode(ledRed, OUTPUT);
  /* initial state is Green */
  greenState = HIGH;
  yellowState = LOW;
  redState = LOW;
}

void loop() {
  /* change state every 500 ms */
  change_state();
  delay(500); // wait 500ms
}

/* Gree -> Yellow -> Red -> Green -> ... */
void change_state() {
  if(greenState == HIGH){
    digitalWrite(ledGreen, LOW);
    digitalWrite(ledYellow, HIGH);
    greenState = LOW;
    yellowState = HIGH;
  }else if(yellowState == HIGH){
    digitalWrite(ledYellow, LOW);
    digitalWrite(ledRed, HIGH);
    yellowState = LOW;
    redState = HIGH;
  }else {
    digitalWrite(ledRed, LOW);
    digitalWrite(ledGreen, HIGH);
    redState = LOW;
    greenState = HIGH;  
  }
}

