//3-2
int times;

void setup(){
  pinMode(11, OUTPUT);
}

void loop(){
  times = analogRead(0) * 4 + 200;
  digitalWrite(11, HIGH);
  delayMicroseconds(times);
  digitalWrite(11, LOW);
  delayMicroseconds(times);
}
