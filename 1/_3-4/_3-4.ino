static int SPEAKER = 13;
static int SWITCH_DO = 12;
static int SWITCH_RE = 11;
static int SWITCH_MI = 10;
static int SWITCH_FA = 9;
static int SWITCH_SO = 8;
static int SWITCH_RA = 7;
static int SWITCH_SI = 6;
static int SWITCH_DOU = 5;

static int DO = 956;
static int RE = 851;
static int MI = 758;
static int FA = 716;
static int SO = 638;
static int RA = 568;
static int SI = 506;
static int DOU = 478;
static int REU = 426;

void setup() {
  pinMode(SWITCH_DO, INPUT);
  pinMode(SWITCH_RE, INPUT);
  pinMode(SWITCH_MI, INPUT);
  pinMode(SWITCH_FA, INPUT);
  pinMode(SWITCH_SO, INPUT);
  pinMode(SWITCH_RA, INPUT);
  pinMode(SWITCH_SI, INPUT);
  pinMode(SWITCH_DOU, INPUT);
  
  pinMode(SPEAKER, OUTPUT);

  /* initialize switches */
  digitalWrite(SWITCH_DO, HIGH);
  digitalWrite(SWITCH_RE, HIGH);
  digitalWrite(SWITCH_MI, HIGH);
  digitalWrite(SWITCH_FA, HIGH);
  digitalWrite(SWITCH_SO, HIGH);
  digitalWrite(SWITCH_RA, HIGH);
  digitalWrite(SWITCH_SI, HIGH);
  digitalWrite(SWITCH_DOU, HIGH);
}

void sound(int tone_delay) {
  digitalWrite(SPEAKER, HIGH);
  delayMicroseconds(tone_delay);
  digitalWrite(SPEAKER, LOW);
  delayMicroseconds(tone_delay);
}

void loop() {
  /* priority: Do > Re > Mi > Fa > So > Ra > Si > DoU */
  /* only one sound */
  if(digitalRead(SWITCH_DO) == LOW)
    sound(DO);
  else if (digitalRead(SWITCH_RE) == LOW)
    sound(RE);
  else if (digitalRead(SWITCH_MI) == LOW)
    sound(MI);
  else if (digitalRead(SWITCH_FA) == LOW)
    sound(FA);
  else if (digitalRead(SWITCH_SO) == LOW)
    sound(SO);
  else if (digitalRead(SWITCH_RA) == LOW)
    sound(RA);
  else if (digitalRead(SWITCH_SI) == LOW)
    sound(SI);
  else if (digitalRead(SWITCH_DOU) == LOW)
    sound(DOU);
}
