int Red = 9;
int Yellow1 = 10;
int Yellow2 = 11;
int Green = 12;
int Wind = 0;

int value;

void setup() {
  pinMode(Red, OUTPUT);
  pinMode(Yellow1, OUTPUT);
  pinMode(Yellow2, OUTPUT);
  pinMode(Green, OUTPUT);
}

void loop() {
  /* Red --> Yellow1 --> Yellow2 --> Green */
  /* Red control */
  if ((value = analogRead(Wind)) == 0) {
    digitalWrite(Red, LOW);
  }
  else {
    digitalWrite(Red, HIGH);
  }

  /* Yelloew1 control */
  if (value < 64) {
    digitalWrite(Yellow1, LOW);
  }
  else {
    digitalWrite(Yellow1, HIGH);
  }

  /* Yellow2 control */
  if (value < 128) {
    digitalWrite(Yellow2, LOW);
  }
  else {
    digitalWrite(Yellow2, HIGH);
  }
  
  /* Green contorl */
  if ( value < 256) {
    digitalWrite(Green, LOW);
  }
  else {
    digitalWrite(Green, HIGH);
  }
}
