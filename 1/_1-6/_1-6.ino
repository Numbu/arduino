static int RED = 4;
static int YELLOW = 3;
static int GREEN = 2;

static int SWITCH_DO = 12;
static int SWITCH_RE = 11;
static int SWITCH_MI = 10;
static int SWITCH_FA = 9;
static int SWITCH_SO = 8;
static int SWITCH_RA = 7;
static int SWITCH_SI = 6;
static int SWITCH_DOU = 5;

int count; // hoe many states are on
int state[7];

void setup() {
  pinMode(SWITCH_DO, INPUT);
  pinMode(SWITCH_RE, INPUT);
  pinMode(SWITCH_MI, INPUT);
  pinMode(SWITCH_FA, INPUT);
  pinMode(SWITCH_SO, INPUT);
  pinMode(SWITCH_RA, INPUT);
  pinMode(SWITCH_SI, INPUT);
  pinMode(SWITCH_DOU, INPUT);
  
  pinMode(RED, OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(GREEN, OUTPUT);

  /* initialize switches */
  digitalWrite(SWITCH_DO, HIGH);
  digitalWrite(SWITCH_RE, HIGH);
  digitalWrite(SWITCH_MI, HIGH);
  digitalWrite(SWITCH_FA, HIGH);
  digitalWrite(SWITCH_SO, HIGH);
  digitalWrite(SWITCH_RA, HIGH);
  digitalWrite(SWITCH_SI, HIGH);
  digitalWrite(SWITCH_DOU, HIGH);
}

void loop() {
  count = 0;
  /* if once switch become "on",
     its state become true and doesn't become off */
  if(digitalRead(SWITCH_DO) == LOW)
    state[0] = true;
  if (digitalRead(SWITCH_RE) == LOW)
    state[1] = true;
  if (digitalRead(SWITCH_MI) == LOW)
    state[2] = true;
  if (digitalRead(SWITCH_FA) == LOW)
    state[3] = true;
  if (digitalRead(SWITCH_SO) == LOW)
    state[4] = true;
  if (digitalRead(SWITCH_RA) == LOW)
    state[5] = true;
  if (digitalRead(SWITCH_SI) == LOW)
    state[6] = true;
  if (digitalRead(SWITCH_DOU) == LOW) {
    /* reset button */ 
    for (int i = 0; i < 7; i++)
      state[i] = false;
  }

  /* how many statea are true */
  for (int i = 0; i < 7; i++) {
    if (state[i] == true)
      count++;
  }

  /* Illminate LEDs based on count */
  if (count > 0)
    digitalWrite(RED, HIGH);
  else
    digitalWrite(RED, LOW);
  if (count > 1)
    digitalWrite(YELLOW, HIGH);
  else
    digitalWrite(YELLOW, LOW);
  if (count > 2)
    digitalWrite(GREEN, HIGH);
  else
    digitalWrite(GREEN, LOW);
}
