//3-1
int times = 500000 / 440; // 880

void setup(){
  pinMode(11,OUTPUT);
}

void loop(){
  digitalWrite(11, HIGH);
  delayMicroseconds(times);
  digitalWrite(11, LOW);
  delayMicroseconds(times);
}
