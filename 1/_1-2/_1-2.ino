int SWITCH = 13; // スイッチの入力をデジタルピン13番に接続
int ledPin = 12; // デジタルピン12番にLEDを接続
int SW_now; // 現在のスイッチの状態
int LED_state;

void setup() {
  pinMode(SWITCH, INPUT );
  pinMode(ledPin, OUTPUT );
  LED_state = LOW; // LEDを最初消灯状態にする
}

void  loop() {
  SW_now = digitalRead(SWITCH); //スイッチの状態を読む
  if(SW_now  == HIGH){ //スイッチの値がHIGHになっているかのチェック
    onPress(); // LEDの状態を変える関数を実行する
  }
}

//スイッチが押された時に，LEDの状態を変える関数
void onPress() {
  if(LED_state  == LOW) {
    digitalWrite(ledPin , HIGH); // LEDを消灯から点灯にする
    LED_state = HIGH;
  }
  else{
    digitalWrite(ledPin , LOW); // LEDを消灯から点灯にする
    LED_state = LOW;
  }
}

