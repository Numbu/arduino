int Red = 11;
int value;
int a;

void setup(){
  pinMode(Red,  OUTPUT);

  /* initialize */
  value = 0;
  a = 25;
}

void loop(){
  analogWrite(Red, value);
  value += a;
  
  /* if value is out of range, change direction */
  if (value > 255 || value < 0) {
    a *= -1;
    value += a;
  }
  delay(100);
}

