int Red = 9;
int Yellow1 = 10;
int Yellow2 = 11;
int Green = 12;
int Wind = 0;

int value; // strenth of wind

void setup() {
  pinMode(Red, OUTPUT);
  pinMode(Yellow1, OUTPUT);
  pinMode(Yellow2, OUTPUT);
  pinMode(Green, OUTPUT);
}

void loop() {
  value = analogRead(Wind);
  analogWrite(Red, constrain(value, 0, 255));
  analogWrite(Yellow1, constrain(value - 64, 0, 255));
  analogWrite(Yellow2, constrain(value - 128, 0, 255));
  analogWrite(Green, constrain(value - 256, 0, 255));
}

