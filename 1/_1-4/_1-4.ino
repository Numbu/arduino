int ledGreen = 10;
int ledRed = 12;
int Green_state;      // Green LEDの点灯状態を表す変数 
int Red_state;        // Red LEDの点灯状態を表す変数
int Green_length = 1; // Green LEDの点灯(消灯)の長さとして            
                      // 最初にセットする値
int Red_length = 3;   // Red LEDの点灯(消灯)の長さとして            
                      // 最初にセットする値
int Green_count;      // Green LED に関する時間経過を表す変数 
int Red_count;        // Red LED に関する時間経過を表す変数

void setup ()
{
  pinMode(ledGreen, OUTPUT); 
  pinMode(ledRed, OUTPUT); 
  Green_state = HIGH;
  Red_state = HIGH; 
  Green_count = 0; 
  Red_count = 0;
}

void loop () {
  /* change green state */
  if(check_time(Green_count, Green_length) == 1){
    /* Green: on -> off */
    if(Green_state == HIGH){
      Green_state = LOW;
      digitalWrite(ledGreen, LOW);
      Green_count = 0;
    }
    
    /* Greeb: off -> on */
    else{
      Green_state = HIGH;
      digitalWrite(ledGreen, HIGH);
      Green_count = 0;
    }
  }

  /* just Green count up */
  else{
    Green_count++;
  }

  /* change Red state */
  if(check_time(Red_count, Red_length)==1){
    /* Red: on -> off */
    if(Red_state == HIGH){
      Red_state = LOW;
      digitalWrite(ledRed, LOW);
      Red_count = 0;
    }

    /* Red: off -> on */
    else{
      Red_state = HIGH;
      digitalWrite(ledRed, HIGH);
      Red_count = 0;
    }
  }

  /* just Red count up */
  else{
    Red_count++;
  }
  
  delay(250); // 250ミリ秒ごとに時間の経過をチェックする 
}

// 時間の経過をチェックするタイマ機能の関数
int check_time (int led_count, int led_length){
  if(led_count == led_length - 1) {
    return 1;
  }else{
    return 0;
  }
}
