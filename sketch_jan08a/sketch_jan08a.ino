//−−−−− 実 験 課 題 3 −−−−−−−
int ledPin = 13;
int interval = 1;
void setup () {
  pinMode(ledPin, OUTPUT);
}
void loop () {
  digitalWrite(ledPin, HIGH);
  delay(interval);
  digitalWrite(ledPin, LOW);
  delay(interval);
  interval++;
}
