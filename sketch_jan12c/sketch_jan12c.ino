//-------実験課題1-2----------//

int SWITCH = 13;
int ledPin = 12;
int SW_now;
int SW_last;

int LED_state;

void setup(){
  pinMode(SWITCH, INPUT);
  pinMode(ledPin,OUTPUT);
  LED_state = LOW;
}

void loop(){
  SW_now = digitalRead(SWITCH);
  if(SW_last == LOW && SW_now == HIGH){
    onPress();
  }
  SW_last = SW_now;
}

void onPress(){
  if(LED_state == LOW){
    digitalWrite(ledPin, HIGH);
    LED_state = HIGH;
  }else{
    digitalWrite(ledPin, LOW);
    LED_state = LOW;
  }
}
