int count = 2;
int counts= 3;
int time = 200;
int SW12;
int SW13;

void setup(){
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, INPUT);
  pinMode(13, INPUT);
}

void loop(){
  digitalWrite(count, LOW);
  digitalWrite(counts, HIGH);
  SW12 = digitalRead(12);
  SW13 = digitalRead(13);
  
  if(SW12 == HIGH && SW13 == HIGH){
    time = 200;
  }else if(SW12 == HIGH){
    time -= 10;
  }else if(SW13 == HIGH){
    if(counts == 7){
      int k;
      for(k = 0;k < 5;k++){
        fanf();
      }
      time = time * 0.6;
    } else {
      time= time * 1.1;
    }
  }
  
  countup();
  
  digitalWrite(7, HIGH);
  delay(time);
}

void countup(){
  count = counts;
  if(counts == 11){
    counts = 2;
  }else{
    counts++;
  }
}

void fanf(){
  int i;
  for(i = 2; i <= 11; i++){
    digitalWrite(i, HIGH);
  }
  delay(200);
  for(i = 2; i <= 11; i++){
  digitalWrite(i, LOW);
  }
  delay(200);
}
