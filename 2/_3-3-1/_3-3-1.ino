int count0;
int count1;
int light0;
int light1;
int a0;
int a1;

int i;

void setup() {
  for (i = 2; i < 12; i++) 
    pinMode(i, OUTPUT);

  count0 = 0;
  count1 = 0;
  light0 = 2;
  light1 = 2;
  a0 = 1;
  a1 = 1;
}

void loop() {
  count0++;
  count1++;

  if (count0 >= 1000) {
    light0 += a0;
    count0 = 0;
    if (light0 > 11) {
      a0 = -1;
      light0 = 10;
    }
    else if (light0 < 2) {
      a0 = 1;
      light0 = 3;
    }
  }

  if (count1 >= 1500) {
    light1 += a1;
    count1 = 0;
    if (light1 > 11) {
      a1 = -1;
      light1 = 10;
    }
    else if (light1 < 2) {
      a1 = 1;
      light1 = 3;
    }
  }

  /* intialize */
  for (i = 2; i < 12; i++)
    digitalWrite(i, LOW);

  digitalWrite(light0, HIGH);
  digitalWrite(light1, HIGH);
}
