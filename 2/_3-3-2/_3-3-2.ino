static int SWITCH = 13;
bool state;
int light;
int a;
int count;

int SW_now;
int SW_last;
long lastDebounceTime = 0;
long debounceDelay = 50;

void setup() {
  for (int i = 2; i < 12; i++) 
    pinMode(i, OUTPUT);
  light = 0;
  a = 1;
  count = 0;
}

void loop() {
  /* prevent chataring */
  SW_now = digitalRead(SWITCH);
  if (SW_last == LOW && SW_now == HIGH) {
    int now = millis();
    if ((now - lastDebounceTime) > debounceDelay) {
      state = !state;
    }
    lastDebounceTime = now;
  }
  SW_last = SW_now;

  count++;
  if (count > 1000) {
    count = 0;
    if (state) {
      light += a;
      if (light < 2) {
	a = 1;
	light = 3;
      }
      else if (light > 11) {
	a = -1;
	light = 10;
      }
      digitalWrite(light, HIGH);
      digitalWrite(light - a, LOW);
    }
  }
}

