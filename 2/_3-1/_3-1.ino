int count0 = 11; // to go out pre LED
int count1 = 2;  // to light next LED

void setup(){
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
}

void loop(){
  digitalWrite(count0, LOW);  // go out pre LED
  digitalWrite(count1, HIGH); // light next LED
  count0 = count1;
  if(count1 == 11) {
    count1 = 2; // jump to another side
  } else {
    count1++;
  }
  delay(200);
}
