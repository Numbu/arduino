#define N 7

int arr[N][10] = {
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {1, 0, 0, 0, 1, 1, 1, 1, 1, 1},
  {1, 0, 0, 0, 1, 1, 0, 0, 0, 1},
  {1, 0, 0, 0, 1, 1, 0, 0, 0, 1},
  {1, 0, 0, 0, 1, 1, 0, 0, 0, 1},
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};
int a, j;
  
void setup() {
  for (int i = 0; i < 10; i++)
    pinMode(i + 2, OUTPUT);
  j = 0;
  a = 1;
}

void loop() {
  j += a;
  for (int i = 0; i < 10; i++)
    digitalWrite(i + 2, arr[j][i]);

  if  (j < 0) {
    a  = 1;
  }
  else if (j >= N) {
    a  = -1;
  }
  
  delay(10);
}
    
